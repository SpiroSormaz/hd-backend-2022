import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Comment } from "./comment.entity";
import { User } from "./user.entity";

@Index("uq_read_comment_comment_id_user_id", ["commentId", "userId"], {
  unique: true,
})
@Index("fk_read_comment_user_id", ["userId"], {})
@Entity("read_comment", { schema: "help_desk" })
export class ReadComment {
  @PrimaryGeneratedColumn({ type: "int", name: "read_comment_id" })
  readCommentId: number;

  @Column("int", { name: "comment_id", unsigned: true, default: () => "'0'" })
  commentId: number;

  @Column("int", { name: "user_id", unsigned: true, default: () => "'0'" })
  userId: number;

  @ManyToOne(() => Comment, (comment) => comment.readComments, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "comment_id", referencedColumnName: "commentId" }])
  comment: Comment;

  @ManyToOne(() => User, (user) => user.readComments, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "userId" }])
  user: User;
}
