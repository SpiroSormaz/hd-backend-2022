import {
  Column,
  Entity,
  Index,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { User } from "./user.entity";
import * as Validator from "class-validator";

@Index("uq_userphoto_image_path", ["imagePath"], { unique: true })
@Index("uq_userphoto_user_id", ["userId"], { unique: true })
@Entity("userphoto", { schema: "help_desk" })
export class Userphoto {
  @PrimaryGeneratedColumn({ type: "int", name: "userPhoto_id", unsigned: true })
  userPhotoId: number;

  @Column("int", {
    name: "user_id",
    unique: true,
    unsigned: true,
    default: () => "'0'",
  })
  userId: number;

  @Column("varchar", {
    name: "image_path",
    unique: true,
    length: 125,
    default: () => "'0'",
  })
  imagePath: string;

  @OneToOne(() => User, (user) => user.userphoto, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "userId" }])
  user: User;
}
