import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Task } from "./task.entity";
import { User } from "./user.entity";

@Index("uq_read_task_task_id_user_id", ["taskId", "userId"], { unique: true })
@Index("fk_read_task_user_id", ["userId"], {})
@Entity("read_task", { schema: "help_desk" })
export class ReadTask {
  @PrimaryGeneratedColumn({ type: "int", name: "read_task_id" })
  readTaskId: number;

  @Column("int", { name: "task_id", unsigned: true, default: () => "'0'" })
  taskId: number;

  @Column("int", { name: "user_id", unsigned: true, default: () => "'0'" })
  userId: number;

  @ManyToOne(() => Task, (task) => task.readTasks, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "task_id", referencedColumnName: "taskId" }])
  task: Task;

  @ManyToOne(() => User, (user) => user.readTasks, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "userId" }])
  user: User;
}
