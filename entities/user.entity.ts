import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  JoinTable,
  ManyToMany,
} from "typeorm";
import { Comment } from "./comment.entity";
import { Client } from "./client.entity";
import { Userphoto } from "./userPhoto.entity";
import * as Validator from "class-validator";
import { ReadTask } from "./read-task.entity";
import { ReadComment } from "./read-comment.entity";
import { Task} from "./task.entity";



@Index("uq_user_email", ["email"], { unique: true })
@Index("fk_user_client_id", ["clientId"], {})
@Entity("user", { schema: "help_desk" })
export class User {
  @PrimaryGeneratedColumn({ type: "int", name: "user_id", unsigned: true })
  userId: number;

  @Column("varchar", { name: "firstname", length: 50, default: () => "'0'" })
@Validator.IsNotEmpty()
@Validator.IsString()
  firstname: string;
  
  @Column("int", { name: "is_removed", default: () => "'0'" })
  isRemoved: number;
  
  @Column("varchar", { name: "lastname", length: 50, default: () => "'0'" })
  @Validator.IsNotEmpty()
@Validator.IsString()
  lastname: string;



  @Column("varchar", {
    name: "password_hash",
    length: 250,
    default: () => "'0'",
  })
  @Validator.IsNotEmpty()
  @Validator.IsHash('sha512')
  passwordHash: string;

  @Column("varchar", {
    name: "email",
    unique: true,
    length: 125,
    default: () => "'0'",
  })
  @Validator.IsEmail()
  email: string;

  @Column("enum", {
    name: "privileges",
    enum: ["super_admin", "admin", "no_privilege"],
    default: () => "'no_privilege'",
  })
  
  privileges: "super_admin" | "admin" | "no_privilege";

  @Column("int", { name: "client_id", nullable: true, unsigned: true })
  clientId: number | null;

 

  @Column("timestamp", {
    name: "user_created_at",
    default: () => "CURRENT_TIMESTAMP",
  })
  userCreatedAt: Date;

  @OneToMany(() => Comment, (comment) => comment.user)
  comments: Comment[];

  @ManyToOne(() => Client, (client) => client.users, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "client_id", referencedColumnName: "clientId" }])
  client: Client;

 

  @OneToOne(() => Userphoto, (userphoto) => userphoto.user)
  userphoto: Userphoto;




  @OneToMany(() => ReadComment, (readComment) => readComment.user)
  readComments: ReadComment[];

  @OneToMany(() => ReadTask, (readTask) => readTask.user)
  readTasks: ReadTask[];





  @ManyToMany(type => Task,task => task.users)
  @JoinTable({
    name:"read_task",
    joinColumn:{name:"user_id",referencedColumnName:"userId"},
    inverseJoinColumn:{name:"task_id",referencedColumnName:"taskId"},
    
  })
  tasks:Task[];
 


}
