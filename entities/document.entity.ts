import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Comment } from "./comment.entity";
import { Task } from "./task.entity";
import * as Validator from "class-validator";

@Index("uq_document_document_path", ["documentPath"], { unique: true })
@Index("uq_document_comment_id", ["commentId"], { unique: true })
@Index("fk_document_task_id", ["taskId"], {})
@Index("fk_comment_comment_id", ["commentId"], {})
@Entity("document", { schema: "help_desk" })
export class Document {
  @PrimaryGeneratedColumn({ type: "int", name: "document_id", unsigned: true })
  documentId: number;

  @Column("int", { name: "task_id", nullable: true, unsigned: true })
  taskId: number | null;

  @Column("int", { name: "comment_id", nullable: true, unsigned: true})
  commentId: number | null;

  @Column("varchar", {
    name: "document_path",
    nullable: true,
    unique: true,
    length: 225,
  })
  documentPath: string | null;

  @ManyToOne(() => Comment, (comment) => comment.documents, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "comment_id", referencedColumnName: "commentId" }])
  comment: Comment;

  @ManyToOne(() => Task, (task) => task.documents, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "task_id", referencedColumnName: "taskId" }])
  task: Task;
}
