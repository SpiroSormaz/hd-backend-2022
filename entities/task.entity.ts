import { Column, Entity, OneToMany, PrimaryGeneratedColumn, ManyToMany, JoinTable, ManyToOne, JoinColumn } from "typeorm";
import { ClientTask } from "./client-task.entity";
import { Comment } from "./comment.entity";
import { Document } from "./document.entity";
import * as Validator from "class-validator";
import { Client } from "./client.entity";
import { ReadTask } from "./read-task.entity";
import { User } from "./user.entity";



@Entity("task", { schema: "help_desk" })
export class Task {
  @PrimaryGeneratedColumn({ type: "int", name: "task_id", unsigned: true })
  taskId: number;

  @Column("timestamp", {
    name: "task_created_at",
    default: () => "CURRENT_TIMESTAMP",
    
  })
  taskCreatedAt: Date;

  @Column("longtext", { name: "task_content" })
  @Validator.IsNotEmpty()
  @Validator.IsString()
  taskContent: string;


  @Column("varchar", { name: "editedStatus",nullable: true})
  @Validator.IsNotEmpty()
  @Validator.IsString()
  @Validator.Length(1,50)
  editedStatus: string;


  @Column("int", { name: "user_id", unsigned: true })
  userId: number;

  @Column("int", { name: "is_removed", default: () => "'0'" })
  isRemoved: number;

  @Column("varchar", { name: "task_title",length:110})
  @Validator.IsNotEmpty()
@Validator.IsString()
taskTitle: string;

  @Column("enum", {
    name: "status",
    enum: ["assigned","notassigned", "paused", "rejected", "closed"],
    default: () => "'notassigned'",
  })
  @Validator.IsNotEmpty()
  @Validator.IsIn(["notassigned", "assigned", "paused", "rejected", "closed"])
 
  status: "notassigned" | "assigned" | "paused" | "rejected" | "closed";

  @ManyToMany(type => Client,client => client.tasks)

  @JoinTable({
    name:"client_task",
    joinColumn:{name:"task_id",referencedColumnName:"taskId"},
    inverseJoinColumn:{name:"client_id",referencedColumnName:"clientId"},
    
  })
  clients:Client[];

  @OneToMany(() => ClientTask, (clientTask) => clientTask.task)
  clientTasks: ClientTask[];

  @OneToMany(() => Comment, (comment) => comment.task)
  comments: Comment[];

  @OneToMany(() => Document, (document) => document.task)
  documents: Document[];




@OneToMany(() => ReadTask, (readTask) => readTask.task)
  readTasks: ReadTask[];





  @ManyToMany(type => User,user => user.tasks)
  @JoinTable({
    name:"read_task",
    joinColumn:{name:"task_id",referencedColumnName:"taskId"},
    inverseJoinColumn:{name:"user_id",referencedColumnName:"userId"},
    
  })
  users:User[];





  @ManyToOne(() => User, (user) => user.tasks, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "userId" }])
  
  user: User;
}
 



