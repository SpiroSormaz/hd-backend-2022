import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Document } from "./document.entity";
import { Task } from "./task.entity";
import { User } from "./user.entity";
import * as Validator from "class-validator";
import { ReadComment } from "./read-comment.entity";

@Index("fk_comment_task_id", ["taskId"], {})
@Index("fk_comment_user_id", ["userId"], {})
@Entity("comment", { schema: "help_desk" })
export class Comment {
  @PrimaryGeneratedColumn({ type: "int", name: "comment_id", unsigned: true })
  commentId: number;


  @Column("timestamp", {
    name: "comment_created_at",
    default: () => "CURRENT_TIMESTAMP",
  })
  
  commentCreatedAt: Date;

  @Column("int", { name: "task_id", unsigned: true, default: () => "'0'" })
  taskId: number;

  @Column("int", { name: "user_id", unsigned: true, default: () => "'0'" })
  userId: number;

  
  @Column("int", { name: "is_removed", default: () => "'0'" })
  isRemoved: number;


  @Column("longtext", { name: "comment_content" })
 @Validator.IsNotEmpty()
 
   commentContent: string;

  @OneToMany(() => Document, (document) => document.comment)
  documents: Document[];

  @ManyToOne(() => Task, (task) => task.comments, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "task_id", referencedColumnName: "taskId" }])
  task: Task;

  @ManyToOne(() => User, (user) => user.comments, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "userId" }])
  user: User;



  @OneToMany(() => ReadComment, (readComment) => readComment.comment)
  readComments: ReadComment[];
}

