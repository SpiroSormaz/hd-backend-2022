import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Client } from "./client.entity";
import { Task } from "./task.entity";
import * as Validator from "class-validator";

@Index("client_task_client_id", ["clientId"], {})
@Index("client_task_task_id", ["taskId"], {})
@Entity("client_task", { schema: "help_desk" })
export class ClientTask {
  @PrimaryGeneratedColumn({
    type: "int",
    name: "client_task_id",
    unsigned: true,
  })
  clientTaskId: number;

  @Column("int", { name: "client_id", unsigned: true, default: () => "'0'" })
  clientId: number;

  @Column("int", { name: "task_id", unsigned: true, default: () => "'0'" })
  taskId: number;

  @ManyToOne(() => Client, (client) => client.clientTasks, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "client_id", referencedColumnName: "clientId" }])
  client: Client;

  @ManyToOne(() => Task, (task) => task.clientTasks, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "task_id", referencedColumnName: "taskId" }])
  task: Task;
}
