import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
} from "typeorm";
import { ClientTask } from "./client-task.entity";


import { Task } from "./task.entity";
import { User } from "./user.entity";
import * as Validator from "class-validator";


@Entity("client", { schema: "help_desk" })
export class Client {
  @PrimaryGeneratedColumn({ type: "int", name: "client_id", unsigned: true })
  clientId: number;

  @Column("varchar", { name: "client_name", length: 50, default: () => "'0'" })
  clientName: string;

  @Column("int", { name: "is_removed", default: () => "'0'" })
  isRemoved: number;


  
  @ManyToMany(type => Task,task => task.clients)
 
  @JoinTable({
    name:"client_task",
    joinColumn:{name:"client_id",referencedColumnName:"clientId"},
    inverseJoinColumn:{name:"task_id",referencedColumnName:"taskId"},
    
  })
  tasks:Task[];
 

  @OneToMany(() => ClientTask, (clientTask) => clientTask.client)
  clientTasks: ClientTask[];

  

  @OneToMany(() => User, (user) => user.client)
  users: User[];
}
