import {
 WebSocketGateway,
 WebSocketServer,
 OnGatewayConnection,
 OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';

@WebSocketGateway(3001)

export class AppGateway implements OnGatewayConnection {

 @WebSocketServer()
 wss;

 handleConnection(client){
    client.emit('connection', 'Success connected to server')
 }

}
