import { Injectable } from "@nestjs/common";
import {TypeOrmCrudService} from "@nestjsx/crud-typeorm";
import {Userphoto } from "entities/userPhoto.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { ApiResponse } from "src/misc/api.response.class";

@Injectable()
export class PhotoService extends TypeOrmCrudService<Userphoto>{

    constructor(
        @InjectRepository(Userphoto)
        private readonly photo:Repository<Userphoto>
    ){
        super(photo)
    }

     add(newPhoto:Userphoto):Promise<Userphoto>{
         const findDuplicate = this.photo.findOne(newPhoto.userId)
        

         
          return this.photo.save(newPhoto)
        

    }

    async delete(id:number){
        return await this.photo.delete(id)
    }
}