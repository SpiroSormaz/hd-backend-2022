import { Injectable, BadRequestException, HttpException, HttpStatus, NotFoundException, Body } from "@nestjs/common";
import { User } from "entities/user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository, IsNull } from "typeorm";
import { UserLoginDto } from "src/dtos/user/login.user.dto";
import { ApiResponse } from "src/misc/api.response.class";
import * as crypto from "crypto";
import { AddUserDto } from "src/dtos/user/add.user.dto";
import { EditUserDto } from "src/dtos/user/edit.user.dto";
import { EditForgottenpassword } from "src/dtos/user/editForgottenPassword.dto";
import { MailerService } from "@nestjs-modules/mailer";
import { MailConfig } from "config/mail.config";
import { EditUserEmailDto } from "src/dtos/user/edit.email.dto";
import { DeleteUserDto } from "src/dtos/user/delete.user.dto";
import { EditFullname } from "src/dtos/user/edit.fullname.dto";

@Injectable()
export class UserService{
    constructor(
        @InjectRepository(User) private readonly user:Repository<User>,
     
        private readonly mailerService:MailerService
    ){}

    //GET MY DATA
    async getDataByEmail(email:string):Promise<User>{
        
            let user  = await this.user.findOne(email);

            if(user === undefined){
                throw new NotFoundException("User not exist")
               
            }else{
                return user;
               
            }
        
    }

         //GET ALL USERS

    async getAll():Promise<User[]>{
      
        const allUser = await this.user
        .createQueryBuilder("user")
        .innerJoinAndSelect("user.client","client")
        .where("user.privileges=:privilege",{privilege:"no_privilege"})
        .getMany()

        return allUser;
    }
  

            //GET ONE BY ID
            getById(id:number):Promise<User | ApiResponse>{
                return new Promise(async(resolve)=>{
                    let user  = await this.user.findOne(id);
        
                    if(user === undefined){
                        resolve(new ApiResponse("error",-1003,"user not exist"))
                    }else{
                        resolve(user);
                    }
                })
            }

            //GET BY EMAIL
async getByEmail(email:string):Promise<User | null>{
    const user = await this.user.findOne
    ({email:email
    })

    if(user){
        return user
    }

    return null;
}



     //ADD NEW USER
 async add(data:AddUserDto):Promise<User | ApiResponse>{
    const passwordHash = crypto.createHash('sha512');
    passwordHash.update(data.password);

    const passwordHashString =  passwordHash.digest('hex').toUpperCase();

    let newUser:User = new User();
  
    newUser.firstname = data.firstname;
    newUser.lastname = data.lastname;
    newUser.passwordHash = passwordHashString;
    newUser.email = data.email;
    newUser.clientId = data.clientId;
    newUser.privileges = data.privileges;
        
        await this.user.save(newUser);


        let user:User = await this.user.findOne({
            where:{
              email:newUser.email,
              
            },
           
           
           // relations:["comments","comments.user","documents","comments.documents"],
           relations:["client"]
     
        })

        return user

}

  //EDIT USER EMAIL
async editUserEmail(id:number,data:EditUserEmailDto):Promise<User>{

    let user : User = await this.user.findOne(id)
    const errMsgNotFoundUser = new Array()
    errMsgNotFoundUser.push('User not exist')
    if(user === undefined){
        throw new NotFoundException(errMsgNotFoundUser)
    }

    const errMsg = new Array()
    errMsg.push("Old email is wrong")
    console.log(user.email +  "-" + data.oldEmail )
    if(user.email !== data.oldEmail){
        throw new BadRequestException(errMsg)

    }

    user.email = data.newEmail; // old email changed

    return this.user.save(user)

}

    async editFullname(id:number, data:EditFullname):Promise<User | ApiResponse>{
        const user :User = await this.user.findOne(id)
        const errMsgNotFoundUser = []
        errMsgNotFoundUser.push('User not exist')

        if(user === undefined){
            throw new NotFoundException(errMsgNotFoundUser);
       }

       user.firstname = data.firstname;
       user.lastname = data.lastname;

       return await this.user.save(user);
    }
    
     //EDIT USER PASSWORD

    async  editUserPassword(id:number,data:EditUserDto):Promise<User | ApiResponse>{
        let user :User = await this.user.findOne(id)
        const errMsgNotFoundUser = []
        errMsgNotFoundUser.push('User not exist')

        if(user === undefined){
            throw new NotFoundException(errMsgNotFoundUser);
            
        }

       
        const passwordHashOld = crypto.createHash("sha512");
        passwordHashOld.update(data.oldPassword)
        const oldForCheck =  passwordHashOld.digest("hex").toUpperCase();
         const errMsg = []
           errMsg.push("Old password is wrong")
         if(user.passwordHash !== oldForCheck){
            throw new BadRequestException(errMsg);

         }

         const passwordHash = crypto.createHash("sha512");
         passwordHash.update(data.password);
         const passwordHashString = passwordHash.digest("hex").toUpperCase();

         user.passwordHash = passwordHashString;

         return this.user.save(user);
     }

     //FORGOTEN PASSWORD (if email is good then send new password on email)
     async  editForgotenPassword(data: EditForgottenpassword):Promise<User | ApiResponse>{
        const user = await this.user.findOne
        ({email:data.email
        })
    
        if(!user){
            throw new NotFoundException('User with this email not exist');
        }
        let randomNumbres:string =
        new Array(6)
      .fill(0)
       .map(e => (Math.random()* 5).toFixed(0).toString()).join('')
 let newPass = randomNumbres

        await this.mailerService.sendMail({
            to:user.email,
            bcc:MailConfig.senderEmail,
            subject:'New password',
            encoding:'UTF-8',
            replyTo:'no-replay@domain.com',
            html:`Your new password is :  <b>${user.email}${newPass}</b> `
        })
    
        const passwordHash = crypto.createHash("sha512");
        passwordHash.update(user.email+newPass);
        const passwordHashString = passwordHash.digest("hex").toUpperCase();

        user.passwordHash = passwordHashString;

        return this.user.save(user);
       
    }

     //DELETE USER old function

     async removeuser(id: number){
        
            return await this.user.delete(id);
       
    }

    //DELETE USER NEW FUNCTION

    async deleteUser(id:number,@Body()data:DeleteUserDto){

        const userForDelete = await this.user.findOne(id)

        if(!userForDelete){
            throw new NotFoundException('User not exist');
        }

        userForDelete.isRemoved = data.isRemoved;

       await  this.user.save(userForDelete)
    }

    //GET ALL USERS WHO NOT REMOVED

    async getAllUserWhoNotRemovedForShow(clientId:number):Promise<User[]>{

        const allUser = await this.user
        .createQueryBuilder("user")
        .where("user.isRemoved=:isRemoved",{isRemoved:0})
        .andWhere("user.clientId=:clientId",{clientId:clientId})
        .getMany()

        if(!allUser){
            throw new NotFoundException('User not exist');

        }

        return allUser;

    }
    async getAllUsers():Promise<User[]>{

      const allUser = await this.user.find();

        if(!allUser){
            throw new NotFoundException('User not exist');

        }

        return allUser;

    }

     async getAllUserWhoNotRemoved(clientId:number, userId:number):Promise<User[]>{
         
        const allUser = await this.user
        .createQueryBuilder("user")
        .where("user.clientId=:clientId",{clientId:clientId})
        .getMany()

        if(!allUser){
            throw new NotFoundException('User not exist');

        }

        return allUser;

    }


    
    async getAllUserWhoNotRemovedWithoutId():Promise<User[]>{
         
        const allUser = await this.user
        .createQueryBuilder("user")
        //.where("user.clientId=:clientId",{clientId:clientId})
        .getMany()

        if(!allUser){
            throw new NotFoundException('User not exist');

        }

        return allUser;

    }


    //GET OUR USERS

    async getAllOurUsersNotRemoved(userId:number):Promise<User[]>{

        const allOurUsers = await this.user
        .createQueryBuilder("user")
        .where("user.userId !=:userId",{userId:userId})
        .andWhere("user.clientId is null")
        .getMany()


        return allOurUsers
    }

      //GET ALL USERS FOR ADMIN SEE

      async getAllUsersNotRemoved():Promise<User[]>{

        const allUsers = await this.user
        .createQueryBuilder("user")
        .leftJoinAndSelect("user.client","client")
         .andWhere("user.clientId is not null")
        .getMany()

         return allUsers
    }

}