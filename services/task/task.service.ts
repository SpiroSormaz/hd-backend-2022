import { Injectable, Res, BadRequestException, Post, Body, NotFoundException, OnModuleInit } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Task } from "entities/task.entity";
import { Repository, createQueryBuilder, getRepository, ConnectionManager, AfterInsert } from "typeorm";
import { ApiResponse } from "src/misc/api.response.class";
import { AddTaskDto } from "src/dtos/task/add.task.dto";
import { ClientTask } from "entities/client-task.entity";
import { EditTaskTitleDto } from "src/dtos/task/changeTaskTitle.dto";
import { EditTaskContent } from "src/dtos/task/changeTaskContent.dto";

import { Comment } from "entities/comment.entity";
import { Document } from "entities/document.entity";
import { DeleteTaskDto } from "src/dtos/task/delete.task.dto";
import { GetAllMyTask } from "src/dtos/task/getAllMyTask.dto";
import { EditTaskContentAndTitle } from "src/dtos/task/changeTaskContentAndTitleTogether.dto";

import { User } from "entities/user.entity";
import {  ReadTaskDto } from "src/dtos/task/readTask.dto";
import { ReadTask } from "entities/read-task.entity";

import { DatabaseConfiguration } from "config/database.configuration";
import { FilterTaskDto } from "src/dtos/task/filter.task.dto";
import { Cron, Interval } from '@nestjs/schedule';
import { ModuleRef } from "@nestjs/core";
import {  FilterTaskForAdminDto } from "src/dtos/task/filterTaskForAdmin.dto";
import { FilterTaskWithStatusDto } from "src/dtos/task/filterTaskWithStatus.dto";
import { MailerService } from "@nestjs-modules/mailer";
import { MailConfig } from "config/mail.config";
import { Client } from "entities/client.entity";
import {createTransport} from 'nodemailer'
import { AppGateway } from "services/socket/app.gateway";


@Injectable()
export class TaskService  {
    constructor(

        @InjectRepository(Task) private readonly task:Repository<Task>,
        @InjectRepository(Client) private readonly client:Repository<Client>,
        @InjectRepository(ClientTask) private readonly clientTask:Repository<ClientTask>,
        @InjectRepository(ReadTask) private readonly readtask:Repository<ReadTask>,
        @InjectRepository(User) private readonly user:Repository<User>,
        private readonly mailerService:MailerService,
        private gateway:AppGateway
       
    ){}
    

    //GET ONE TASK BY ID AND HIS COMMENTS
    async getOneById(id:number):Promise<Task | ApiResponse>{
         
        const task:Task  = await this.task.findOne({
            where:{
              taskId:id,
              
            },
                    
           // relations:["comments","comments.user","documents","comments.documents"],
           relations:["documents","user"]   
            
        })
    
            if(task === undefined){

                throw new NotFoundException('Ticket not exist');
               }
            return task;         
    }


    //write parameter in readTask table for notification

  sendParametersInReadTaskTable(data:ReadTaskDto,id):Promise<ReadTask | ApiResponse>{
      
    const newReadTask:ReadTask = new ReadTask();

    newReadTask.userId = id;
    newReadTask.taskId  = data.taskId

    return this.readtask.save(newReadTask)

 }


 //get task for notification

 async findTaskForNotification(userId:number): Promise<Task[]>{
   const taskForNot = await this.task.query("call getTaskForNotification(?)", [userId])
  
   return taskForNot
  
}

async handleInterval(userId) {
    
      const taskForNot = await this.task.query("call getTaskForNotification(?)", [userId])
    return taskForNot
      
}

// get task for notification for admin

async findTaskForNotificationForAdmin(userId:number):Promise<Task[]>{
    const taskForNotForAdmin = await this.task.query("call getTaskNotificationForAdmin(?)",[userId])
    
    return taskForNotForAdmin

}

/*
web Socket

1. Uspostaviti konekciju - zavrseno
2. Staviti sve konektovane korisnike u jedan niz
3. Proci kroz podatke svakog korisnika i proveriti njegov id
4. Zatim na svakih 10 sec pozvati proceduru koja proverava taskove koji nisu procitani
5. Poslati taskove korisniku 

*/

        //ADD NEW TASK

        async addNewTask(@Body()data:AddTaskDto):Promise<Task | ApiResponse>{
            const newTask:Task = new Task();

            newTask.taskContent = data.taskContent;
            newTask.taskTitle = data.taskTitle
            newTask.status = data.status;
            newTask.userId = data.userId;
            
            const savedTask = await this.task.save(newTask)
    
 
            const client = await this.client.findOne(data.clientId)

            const dataNotifyObject = {
                user:savedTask,
                client:client
            }

            this.gateway.wss.emit('ticketNotification', dataNotifyObject);
           

            const admins = await this.user.createQueryBuilder("user")
            .where("user.privileges =:privileges",{privileges:"admin"})
            .getMany();
            let allAdminsEmail;
            admins.forEach(element => {
            allAdminsEmail = element.email
                
            });
            const transporter =  createTransport({
                host:MailConfig.hostname,
                requireTLS:true,
                port: 587,
                secure:false, // true for 465, false for other ports
                auth: {
                  user:MailConfig.username, // generated ethereal user
                  pass: MailConfig.password, // generated ethereal password
                },
              });
            
              // send mail with defined transport object
              const info ={
                from: 'ticket@insolutions.tech', // sender address
                to: allAdminsEmail, // list of receivers
                subject: "HD-APP ✔", // Subject line
                text: "HD-TICKET", // plain text body
                html: `${client.clientName} created ticket:<br><br>
                <b>Title:</b><br>
                ${newTask.taskTitle}<br><br>
                <b>Content:</b><br>
                ${newTask.taskContent}`, // html body
              };
              transporter.sendMail(info)
            

            /* na osnovu id clienta cu da uzmem client name*/
/*
          this.mailerService.sendMail({
            
                to:allAdminsEmail,
                from:'ticket@insolutions.tech', 
                bcc:MailConfig.senderEmail,
                subject:'New Ticket',
                sender:"HD-APP",
                encoding:'UTF-8',
                replyTo:'no-replay@domain.com',
                html:`${client.clientName} created ticket:<br><br>
                <b>Title:</b><br>
                ${newTask.taskTitle}<br><br>
                <b>Content:</b><br>
                ${newTask.taskContent}`
            })*/
    
            const newUserTask:ClientTask = new ClientTask();
    
         newUserTask.taskId = savedTask.taskId
         newUserTask.clientId = data.clientId;                                                                          
         this.clientTask.save(newUserTask)
         return savedTask;
    
    }

     //GET ALL MY TASKS
    async getAllById(id:number):Promise<Task[] | ApiResponse>{
       
//test 
const tasks = await this.task
.createQueryBuilder("task")
.innerJoinAndSelect("task.clientTasks","clients")
.innerJoinAndSelect("task.user","user")
.innerJoinAndSelect("task.clients","client")
.where("clients.clientId =:id",{id:id})
.andWhere("task.isRemoved !=:isRemoved",{isRemoved:1})
.orderBy('task.taskCreatedAt', 'DESC')
.getMany();


return tasks

//test


/*

      const clientTasks = await this.clientTask.find({
            where:{
                clientId:id,
                
            },
            relations:[
                "task",
               
               ],
})

if(clientTasks.length === 0){  //IF TASK NOT EXIST
    throw new NotFoundException('There are no task');
}
        return clientTasks;
        */

 }
         
        //CHANGE TASK STATUS
         
         async getById(taskId:number){
             return await this.task.findOne(taskId)
         }


         async changeTaskStatus(taskId:number,newStatus:"notassigned" | "assigned" | "paused" | "rejected" | "closed",editedStatus:string):Promise<Task | ApiResponse>{
             const taskForChangeStatus = await this.getById(taskId)

             if(!taskForChangeStatus){
                throw new NotFoundException('Ticket not exist');
             }

             taskForChangeStatus.status = newStatus;
             taskForChangeStatus.editedStatus = editedStatus;

            

             await this.task.save(taskForChangeStatus)

             return await this.getById(taskId)
         }

         //EDIT TASK TITLE

         async editTaskTitle(taskId:number,data:EditTaskTitleDto){
            const taskForChangeTitle = await this.getById(taskId)

            if(!taskForChangeTitle){
                throw new NotFoundException('Ticket not exist');
             }
            
            if(data.userId != taskForChangeTitle.userId){
                throw new BadRequestException('You do not have permission to edit ticket title');

            }
                       
            taskForChangeTitle.taskTitle = data.newTitle
            await this.task.save(taskForChangeTitle)
            return await this.getById(taskId)

         }

         //EDIT TASK CONTENT

         async editTaskContent(taskId:number,data:EditTaskContent){

            const taskForChangeContent = await this.getById(taskId)

            if(!taskForChangeContent){

                throw new NotFoundException('Ticket not exist');


            }

            if(data.userId != taskForChangeContent.userId){
                throw new BadRequestException('You do not have permission to edit ticket content');

            }
                    

            taskForChangeContent.taskContent = data.newContent;

            await this.task.save(taskForChangeContent)

            return await this.getById(taskId)

         }

        //EDIT TASK CONTENT AND TITLE TOGETHER
        async editTaskContentAndTitleTogether(taskId:number,data:EditTaskContentAndTitle){
            const taskForEdit = await this.getById(taskId)
        
            if(!taskForEdit){

                throw new NotFoundException('Ticket not exist');


            }

            if(data.userId != taskForEdit.userId){
                throw new BadRequestException('You do not have permission to edit task content');

              
            }
            taskForEdit.taskContent = data.newContent;
            taskForEdit.taskTitle = data.newTitle;
            await this.task.save(taskForEdit)
            return await this.getById(taskId)
                   
        
        }

         ////GET ALL MY  TASK WITH THE STATUS I WANT

       async  getAllMyTaskStatusIWant(status:string,id:number):Promise<Task[]>{

        const tasks = await this.task
        .createQueryBuilder("task")
        .leftJoinAndSelect("task.clients","clients")
        .leftJoinAndSelect("task.user","user")
        .where("clients.clientId =:id",{id:id})
        .andWhere("task.isRemoved !=:isRemoved",{isRemoved:1})
        .andWhere("task.status =:status",{status:status})
        .orderBy('task.taskCreatedAt', 'DESC')
        .getMany();

        return tasks
    
}

    ////GET ALL MY  TASK WITH THE STATUS I WANT without clientId

    async  getAllMyTaskStatusIWantVerTwo(status:string):Promise<Task[]>{

        const tasks = await this.task
        .createQueryBuilder("task")
        .leftJoinAndSelect("task.clients","clients")
        .leftJoinAndSelect("task.user","user")
        .where("task.isRemoved !=:isRemoved",{isRemoved:1})
        .andWhere("task.status =:status",{status:status})
        .orderBy('task.taskCreatedAt', 'DESC')
        .getMany();
        
        return tasks
    
}


//DELETE TASK
/*
async removeTask(id:number,@Body()data:DeleteTaskDto){

    const taskForDelete = await  this.task.findOne(id)
    
    if( taskForDelete.userId != data.userId){
       return

    }
    
    const clientTasks = await this.clientTask.delete(id)
    const documentsForDel = await this.document.createQueryBuilder("document").select("document","task")
    .where("document.taskId = :id" ,{id:id}).getMany()  
   this.document.remove(documentsForDel)

   const commentsForDel = await this.comment.createQueryBuilder("comment").select("comment","task")
   .where("comment.taskId = :id",{id:id}).getMany()

   this.comment.remove(commentsForDel)
    
   setTimeout(() => {
        this.task.delete(id)
        
    }, 700);
    return [clientTasks,documentsForDel,commentsForDel]
    
   
    
    }*/

    //Get all my tasks 

   async getAllMyTask(@Body()data:GetAllMyTask):Promise<Task[]>{
   
    const tasks = await this.task
    .createQueryBuilder("task")
    .where("task.userId =:userId", { userId: data.userId })
    .andWhere("task.isRemoved !=:isRemoved",{isRemoved:1})
    .orderBy('task.taskCreatedAt', 'DESC')
    .getMany();
   return tasks

}

//DELETE TASK BUT NOT FROM DB, ONLY IS CHANGED PARAMETER isRemoved

async removeTask(id:number,@Body()data:DeleteTaskDto){

    const taskForDelete = await  this.task.findOne(id)
    
    if( taskForDelete.userId != data.userId){
        throw new NotFoundException('Ticket not exist');

    }else{
        taskForDelete.isRemoved = data.isRemoved
        await this.task.save(taskForDelete)

    }


    }

    //filter task

    async search(data:FilterTaskDto):Promise<Task[]>{
        
        const regExForCheck = /[a-zA-Z]+/g;
        const builder = await this.task.createQueryBuilder("task")
        

      

        if(!data.forSearchTask.match(regExForCheck)){
            builder.innerJoinAndSelect("task.users","user")
            builder.where("task.taskId Like :kw",{kw:'%' + data.forSearchTask + '%'})
            builder.andWhere("user.clientId=:clientId",{clientId:data.clientId})
           
        }
        
         if(data.forSearchTask.match(regExForCheck)){
            builder.innerJoinAndSelect("task.users","user")
            builder.where("task.taskTitle Like :kw",{kw:'%' + data.forSearchTask + '%'})
            builder.andWhere("user.clientId=:clientId",{clientId:data.clientId})
            
            
          }

          if(data.forSearchTask.trim().length == 0 ){
            
            builder.where("task.taskTitle Like :kw",{kw:'%' + data.forSearchTask + '1%'})
            builder.andWhere("user.clientId=:clientId",{clientId:data.clientId})
           
          }


         const items = await builder.getMany()
                    
        return items
    }



    //FILTER FOR ADMIN FOR ALL TASKS

    async filterForAdmin(data:FilterTaskForAdminDto):Promise<Task[]>{

        const regExForCheck = /[a-zA-Z]+/g
        
        const builder = await this.task.createQueryBuilder("task")
     
       builder.innerJoinAndSelect("task.users","user")
        if(!data.forSearchTask.match(regExForCheck)){
            builder.where("task.taskId Like :kw",{kw: '%' + data.forSearchTask + '%'})
            
            }
        

        if(data.forSearchTask.match(regExForCheck)){
             builder.where("task.taskTitle Like :kw",{kw:'%' + data.forSearchTask + '%'})

            
        }

        if(data.forSearchTask.trim().length == 0 ){
            
            builder.where("task.taskTitle Like :kw",{kw:'%' + data.forSearchTask + '%'})
          
          }


         const items = await  builder.getMany()
                    
        return items

    }
    
    //GET ALL TASKS WHO IS NOT REMOVED (FOR ADMIN)


    async getAllTasksForShow():Promise<Task[] | Task>{
        const tasks = await this.task
        .createQueryBuilder("task")
        .leftJoinAndSelect("task.user","user")
        .leftJoinAndSelect("task.clients","client")
        .where("task.isRemoved = :isRemoved",{isRemoved:0})
        .orderBy('task.taskCreatedAt','DESC')
        .getMany();

        if(!tasks){
            throw new NotFoundException('Ticket not exist');
        }

       return tasks

    }

    //filter task with status for client

    async searchTaskForClient(data:FilterTaskWithStatusDto):Promise<Task[]>{
       
        const builder = await this.task.createQueryBuilder("task")
          //var regExForCheck = /[a-zA-Z]+/g
        
      builder.innerJoinAndSelect("task.user","user")
       
            
            
           // builder.andWhere('task.status =:status',{status:data.status});
           
            builder.andWhere('user.clientId =:clientId',{clientId:data.clientId});
     
       // if(!data.forSearchTask.match(regExForCheck)){
            builder.andWhere("task.taskId Like :kw",{kw: '%' + data.forSearchTask + '%'})

        //}

       // if(data.forSearchTask.match(regExForCheck)){
           // builder.andWhere("task.taskTitle Like :kw",{kw:'%' + data.forSearchTask + '%'})
     
        //}
          if(data.forSearchTask.trim().length == 0 ){         
            return null
           
          }


         const items = await  builder.getMany()
                    
          return items
    }

    } 





 
 
  

 

