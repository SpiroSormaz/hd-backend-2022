import { Injectable } from "@nestjs/common";
import {TypeOrmCrudService} from "@nestjsx/crud-typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Document } from "entities/document.entity";


@Injectable()
export class DocumentService extends TypeOrmCrudService<Document>{

    constructor(
        @InjectRepository(Document)
        private readonly document:Repository<Document>
    ){
        super(document)
    }

    //UPLOAD DOCUMENT

     async add(newDocument:Document):Promise<Document>{
         const findDuplicate = this.document.findOne(newDocument.commentId)
         return await this.document.save(newDocument)
    }
        
    //DELETE DOCUMENT
    async delete(id:number){
        return await this.document.delete(id)
    }

/*

    async getByDocumentPath(documentPath:string):Promise<Document| null>{
        const document = await this.document.findOne
        ({documentPath:documentPath
        })
    
        if(document){
            return document
        }
    
        return null;
    }
    */
        
    
}