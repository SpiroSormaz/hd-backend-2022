import { Injectable, Param, Body, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Client } from "entities/client.entity";
import { Repository } from "typeorm";
import { AddClientDto } from "src/dtos/client/add.client.dto";
import { DeleteClientDto } from "src/dtos/client/delete.client.dto";

@Injectable()
export class ClientService{

    constructor(

        @InjectRepository(Client) private readonly client:Repository<Client>,
    ){}

    //GET ALL CLIENT
    async getAllClient():Promise<Client[]>{
           var clients = await this.client.
           createQueryBuilder("client")
           .getMany() 

           if(!clients){
            throw new NotFoundException('Client not exist');

           }

           return clients
    }


    //ADD NEW CLIENT

    addNewClient(data:AddClientDto):Promise<Client>{

        let newClient = new Client()

        newClient.clientName = data.clientName;
       

        return this.client.save(newClient)

    }


    //GET CLIENT NAME

   async getClientName(clientId:number):Promise<Client>{
     return await this.client.findOne(clientId)

      
    }


    //DELETE CLIENT

  async deleteClient(id:number,@Body()data:DeleteClientDto){
      
    const clientForDelete = await this.client.findOne(id)


    if(!clientForDelete){
        throw new NotFoundException('Client not exist');
    }


    clientForDelete.isRemoved = data.isRemoved

          await this.client.save(clientForDelete)


  }
       

       async getAllClientsForAdmin():Promise<Client[]>{
          
      const clients = await this.client.find()

      if(!clients){
        throw new NotFoundException('Client not exist');

      }

      return clients;
           
       }



       async getAllClients():Promise<Client[]>{

        const all = await this.client.createQueryBuilder("client")
       .getMany()

        return all
        
       }


}
