import { Injectable, BadRequestException, NotFoundException, Body } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Comment } from "entities/comment.entity";
import { Repository } from "typeorm";
import { AddCommentDto } from "src/dtos/comment/add.comment.dto";
import { ApiResponse } from "src/misc/api.response.class";
import { EditCommentDto } from "src/dtos/comment/editComment.dto";
import { DeleteCommentDto } from "src/dtos/comment/delete.comment.dto";
import { Document } from "entities/document.entity";
import { ReadCommentDto } from "src/dtos/comment/readComment.dto";
import { ReadComment } from "entities/read-comment.entity";
import { MailerService } from "@nestjs-modules/mailer";
import { MailConfig } from "config/mail.config";
import { AppGateway } from "services/socket/app.gateway";



@Injectable()
export class CommentService{
    constructor(
       @InjectRepository(Comment)private readonly comment:Repository<Comment>,
       @InjectRepository(Document)private readonly document:Repository<Document>,
       @InjectRepository(ReadComment) private readonly readComment:Repository<ReadComment>,
       private readonly mailerService:MailerService,
       private readonly gateway:AppGateway,

    ){}


       //GET ALL COMMENTS from one task

       async getAllByTaskId(id:number):Promise<Comment[]>{
        
         const commentsFromTask = await this.comment.find({
         where:{
                 taskId:id,
                 isRemoved:0
                 
                 
             },
             order: {
                commentCreatedAt:"DESC"
            },
           
           
             
             relations:[
                 "documents","user"
                
                ],
               
    })
         return commentsFromTask;
      
  }

  // get one by id  (only content)

  async getOneCommentById(id:number){

    const comment = await this.comment.findOne(id)

    if(!comment){
        throw new NotFoundException("Comment not exist")
    }
    return comment.commentContent
  }

    // get one by id  (all data)

    async getOneCommentByIdAllData(id:number){
       const comment = await this.comment.findOne(id)
           if(!comment){
            throw new NotFoundException("Comment not exist")
           }
            return comment
      }


      //write parameter in readTComment table for notification

      sendParametersInReadCommentTable(data:ReadCommentDto, id):Promise<ReadComment | ApiResponse>{
      
        let newReadComment:ReadComment = new ReadComment();
    
        newReadComment.userId = id;
        newReadComment.commentId  = data.commentId
    
        return this.readComment.save(newReadComment)
    
     }

      //get comment for notification

 async findCommentForNotification(userId:number): Promise<Comment[]>{
    const commentForNot = await this.comment.query("call getCommentForNotification(?)", [userId,"user not exist"])
 
    return commentForNot
 }

     //get comment for notification

     async findCommentForNotificationForAdmin(userId:number): Promise<Comment[]>{
        const commentForNotForAdmin = await this.comment.query("call getCommentNotificationForAdmin(?)", [userId])
     
        return commentForNotForAdmin
     }
     
       //ADD NEW COMMENT

      async  addComment(data:AddCommentDto):Promise<Comment | ApiResponse>{

        let newComment :Comment = new Comment();

        newComment.taskId = data.taskId;
        newComment.userId = data.userId;
        newComment.commentContent = data.commentContent;
        await  this.comment.save(newComment);
        const commentNew = await this.comment.findOne({
            where:{
                commentId:newComment.commentId,
                
                
                
            },
            relations:[
                "user"
            ],
        })
  
       this.gateway.wss.emit("commentNotification", commentNew);
        return commentNew
       }

       //FIND COMMENTT

       async getById(commentId:number){
        return await this.comment.findOne(commentId)
    }

       //EDIT COMMENT

       async editComment(commentId:number,data:EditCommentDto){

          const commentForEdit = await this.getById(commentId)
          if(commentForEdit.userId!= data.userId){
            return
        }

          if(!commentForEdit){
              throw new NotFoundException("Comment not exist")
          }

          commentForEdit.commentContent = data.newContent

          await this.comment.save(commentForEdit)
          return await this.getById(commentId)
          
       }

      
        //DELETE COMMENT FROM DB
/*
        async removeComment(id:number,@Body()data:DeleteCommentDto){
            
            const commentForDelete = await  this.comment.findOne(id)

            if(commentForDelete.userId != data.userId){
                return
            }
    
            const documentsForDel = await this.document.createQueryBuilder("document").select("document","task")
            .where("document.commentId = :id" ,{id:id}).getMany()  
           this.document.remove(documentsForDel)
            
            

            setTimeout(() => {
             this.comment.delete(id)

            }, 500);
        return documentsForDel
        
        
 }
*/
     
        //DELETE COMMENT BUT NOT FROM DB ONLY IS CHANGED PARAMETER isRemoved

        async removeComment(id:number,@Body()data:DeleteCommentDto){
            
            const commentForDelete = await  this.comment.findOne(id)

            if(commentForDelete.userId != data.userId){
                return
            }else{
                commentForDelete.isRemoved = data.isRemoved
                   await this.comment.save(commentForDelete)
            }
  
 }
}