export const TaskDocumentStorageConfig = {
    documentDestination:'../storage/task_documents/',
    urlPrefix:'/assets/task_document',
    maxAge:1000 * 60 * 60 * 24 * 7,
    documentMaxFileSize:1024 * 1024 * 500 //=300MB
};




