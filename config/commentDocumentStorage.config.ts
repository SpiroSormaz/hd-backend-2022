export const CommentDocumentStorageConfig = {
    documentDestination:'../storage/comment_documents/',
    urlPrefix:'/assets/comment_document',
    maxAge:1000 * 60 * 60 * 24 * 7,
    documentMaxFileSize:1024 * 1024 * 500 //=300MB
};





