import { Controller, Post, Body, Get, Param, Delete, Patch,  Req } from "@nestjs/common";
import { CommentService } from "services/comment/comment.service";
import { AddCommentDto } from "src/dtos/comment/add.comment.dto";
import { ApiResponse } from "src/misc/api.response.class";
import { Comment } from "entities/comment.entity";
import { EditCommentDto } from "src/dtos/comment/editComment.dto";
import { DeleteCommentDto } from "src/dtos/comment/delete.comment.dto";
import { ReadCommentDto } from "src/dtos/comment/readComment.dto";
import { ReadComment } from "entities/read-comment.entity";

@Controller('api/comment')

export class CommentController{
    constructor(
        public commentService:CommentService
    ){}



     //ADD COMMENT
    @Post('addComment')
    addComment(@Body()data:AddCommentDto):Promise<Comment | ApiResponse>{
        return this.commentService.addComment(data)

    }



    //GET ALL COMMENTS FROM ONE TASK

    @Get(":id/allCommentsFromTask")
async getMyTasks(@Param("id")taskId:number):Promise<Comment[]>{
    return this.commentService.getAllByTaskId(taskId);
    
   }



   //GET ONE BY ID

   @Get("/:id/getOneCommentById")
   getOneById(@Param("id")id:number){
       return this.commentService.getOneCommentById(id)
   }



   //EDIT COMMENT CONTENT

   @Post(":id/editComment")
   async changeContent(@Param('id')id:number,@Body()data:EditCommentDto){
       return await this.commentService.editComment(id,data)
   }




   //DELETE COMMENT


   @Post(':id/deleteComment')
  remove(@Param('id') id: number,@Body()data:DeleteCommentDto):void{
    this.commentService.removeComment(id,data)
    
  }


//dont show me notif for this task 
  @Post("sendReadComment")
  sendParametersInReadCommentTableForTask(@Req()req,@Body()data:ReadCommentDto):Promise<ReadComment | ApiResponse>{
    return this.commentService.sendParametersInReadCommentTable(data,req.token.userId)
  }



//get comment for notification
  @Post('getCommentForNotification')
  async find(@Req()req): Promise <Comment[]>{
      return await this.commentService.findCommentForNotification(req.token.userId);
  
}


//get comment for notification for admin
@Post('getCommentForNotificationForAdmin')
async findCommentForNotificationForAdmin(@Req()req): Promise <Comment[]>{
    return await this.commentService.findCommentForNotificationForAdmin(req.token.userId);

}


  
  


}