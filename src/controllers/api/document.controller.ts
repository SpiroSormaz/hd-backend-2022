import { Controller, Param, Post, UseInterceptors, UploadedFile,Req, Delete, HttpException, HttpStatus, UploadedFiles, Get, Res,Header} from "@nestjs/common";
import { CommentDocumentStorageConfig } from "config/commentDocumentStorage.config";
import { ApiResponse } from "src/misc/api.response.class";
import { Document } from "entities/document.entity";
import { DocumentService } from "services/document/document.service";
import { FileInterceptor, FilesInterceptor } from "@nestjs/platform-express";
import {diskStorage} from "multer"; // this must be install in npm
import {unlinkSync}  from  "fs";
import { TaskDocumentStorageConfig } from "config/taskDocumentStorage.config";
import { CommentService } from "services/comment/comment.service";

@Controller('document')
export class DocumentController{
    constructor(
        public documentService:DocumentService,
        public commentService:CommentService
    ){}


//DELETE DOCUMENT
@Delete('deleteDocument/:documentId/:commentId')
public async deletePhoto(
    @Param('commentId') commentId:number,
    @Param('documentId')documentId:number,
    ){
        
      //chech if user exist

      const comment = await this.documentService.findOne({
          commentId:commentId,
          documentId:documentId,
      })
      if(!comment){
        throw new HttpException({
            message: /*err.message*/"Comment not found"
          }, HttpStatus.NOT_FOUND);
       }
try{
       unlinkSync(CommentDocumentStorageConfig.documentDestination + comment.documentPath)
} catch(e){
   
}
      const deleteResult = await this.documentService.delete(documentId)

        if(deleteResult.affected == 0){  //How many photo have been deleted
            throw new HttpException({
                message: /*err.message*/"Document not found"
              }, HttpStatus.NOT_FOUND);
        }  

        return new ApiResponse("ok",0,"One document is deleted")
  
  }


//UPLOAD DOCUMENT IN TASK
@Post(':taskId/uploadDocumentInTask')

@UseInterceptors(
  FilesInterceptor('document', 10, {
    storage: diskStorage({
      destination: TaskDocumentStorageConfig.documentDestination,
      filename:(req,file,callback)=>{
  
        let original:string = file.originalname;

        let normalized = original.replace(/\s+/g,'s')//replace all spaces with a  dash
        
        //make date like "20200426"
        let current = new Date();
        let datePart = '';

        datePart+= current.getFullYear().toString();
        datePart+= (current.getMonth() + 1).toString();
        datePart+=current.getDate().toString();
         

        //make random number like "9547612539"
     
        let randomPart:string =
         new Array(10)
        .fill(0)
        .map(e => (Math.random()* 9).toFixed(0).toString()).join('')
        let fileName = datePart + '-' + randomPart + '-' + normalized
         callback(null, fileName)

       }
    }),
    limits:{
    
      fileSize:TaskDocumentStorageConfig.documentMaxFileSize
  }

  }),
)
async uploadMultipleFiles(@Param('taskId')taskId:number,@UploadedFiles() document) {
  
  document.forEach(file => {
    const fileReponse = {
      originalname: file.originalname,
      filename: file.filename,
    };
    const newDocument:Document= new Document();
    newDocument.taskId = taskId
    newDocument.documentPath = file.filename;
    try{ 
      var savedDocument =  this.documentService.add(newDocument);
     } catch (err) {
      
     }
     return savedDocument;
  });
}

/*

//UPLOAD DOCUMENT IN COMMENT
@Post(':commentId/uploadDocumentInComment')
 @UseInterceptors(
  FilesInterceptor('document', 10, {
    storage: diskStorage({
      destination: CommentDocumentStorageConfig.documentDestination,
      filename:(req,file,callback)=>{
  
        let original:string = file.originalname;
        let normalized = original.replace(/\s+/g,'s')//replace all spaces with a  dash
        
        //make date like "20200426"
        let current = new Date();
        let datePart = '';

        datePart+= current.getFullYear().toString();
        datePart+= (current.getMonth() + 1).toString();
        datePart+=current.getDate().toString();
         
     //make random number like "9547612539" 
        let randomPart:string =
         new Array(10)
       .fill(0)
        .map(e => (Math.random()* 9).toFixed(0).toString()).join('')
        let fileName = datePart + '-' + randomPart + '-' + normalized
        callback(null, fileName)

       }      
    }),
    limits:{  
      fileSize:CommentDocumentStorageConfig.documentMaxFileSize
  } 
  }),
)
async uploadMultipleFilesInComment(@Param('commentId')commentId:number,@UploadedFiles() document) {

  document.forEach(async file => {
    const fileReponse = {
      originalname: file.originalname,
      filename: file.filename,
    };
     
    const newDocument:Document= new Document();
    newDocument.commentId = commentId 
    newDocument.documentPath = file.filename;

    try{ 
        const res = await this.documentService.add(newDocument); 
        return fileReponse;
     } catch (err) {
         return err;
     }     
  });
}

*/

@Post(':commentId/uploadDocumentInComment')
@UseInterceptors(
  FilesInterceptor('document', 10, {
    storage: diskStorage({
      destination:CommentDocumentStorageConfig.documentDestination,
      filename:(req,file,callback)=>{
  
        let original:string = file.originalname;
        let normalized = original.replace(/\s+/g,'s')//replace all spaces with a  dash
        
        //make date like "20200426"
        let current = new Date();
        let datePart = '';

        datePart+= current.getFullYear().toString();
        datePart+= (current.getMonth() + 1).toString();
        datePart+=current.getDate().toString();
         
     //make random number like "9547612539" 
        let randomPart:string =
         new Array(10)
       .fill(0)
        .map(e => (Math.random()* 9).toFixed(0).toString()).join('')
        let fileName = datePart + '-' + randomPart + '-' + normalized
        callback(null, fileName)

       }  
    }),
  
  }),
)
async uploadFiles(@Param('commentId')commentId:number,@UploadedFiles() document) {
  const response = [];
  document.forEach(async file => {
    const fileReponse = {
      originalname: file.originalname,
      filename: file.filename,
    };

    const newDocument:Document= new Document();
    newDocument.commentId = commentId 
    newDocument.documentPath = file.filename;

    response.push(fileReponse);
        await this.documentService.add(newDocument); 
        return fileReponse;
 
    
    
  });
  return response;
}

@Get(":imgpath")
@Header('Cache-Control', 'must-revalidate')
@Header('Expires', '0')
//@Header('Content-Type' ,'application/octet-stream')


seeUploadedFile(@Param("imgpath")document,
@Res()res){
  return res.sendFile(document,{root:"../storage/task_documents"}) 
}


@Get("commentFile/:imgpath")
@Header('Cache-Control', 'must-revalidate')
@Header('Expires', '0')
//@Header('Content-Type' ,'application/octet-stream')


seeUploadedCommentFile(@Param("imgpath")document,
@Res()res){
  return res.sendFile(document,{root:"../storage/comment_documents"}) 
 } 
}
