import { Controller, Post, Param, Body } from "@nestjs/common";
import { UserService } from "services/user/user.service";
import { EditForgottenpassword } from "src/dtos/user/editForgottenPassword.dto";
import { User } from "entities/user.entity";
import { ApiResponse } from "src/misc/api.response.class";
import { MailerService } from "@nestjs-modules/mailer";


@Controller()
export class ForgottenPasswordController{
    constructor(
        private userService:UserService
        
    ){}



    //EDIT FORGOTEN PASSWORD

    @Post('forgottenPassword')
  async editForgotenPassword(@Body()data: EditForgottenpassword):Promise<User | ApiResponse>{
    
   const user =  await this.userService.editForgotenPassword(data);


   return user;
    
   }

}