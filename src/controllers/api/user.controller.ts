import { Controller, Get, Post, Body, Put, Param, UseInterceptors, UploadedFile, Req, Delete, SetMetadata, UseGuards, HttpException, HttpStatus } from "@nestjs/common";
import { UserService } from "services/user/user.service";
import { User } from "entities/user.entity";
import { UserLoginDto } from "src/dtos/user/login.user.dto";
import { ApiResponse } from "src/misc/api.response.class";
import { AddUserDto } from "src/dtos/user/add.user.dto";
import {FileInterceptor, FileFieldsInterceptor } from "@nestjs/platform-express"
import {diskStorage} from "multer"; // this must be install in npm
import { StorageConfig } from "config/storage.config";
import { PhotoService } from "services/photo/photo.service";
import { Userphoto } from "entities/userPhoto.entity";
import {fromFile}  from "file-type";
import {unlinkSync}  from  "fs";
import { AllowToRoles } from "src/misc/allow.to.roles.descriptor";
import { RoleCheckedGuard } from "src/misc/role.checker.guard";
import { EditUserDto } from "src/dtos/user/edit.user.dto";
import  { Request } from "express"
import { EditUserEmailDto } from "src/dtos/user/edit.email.dto";
import { DeleteUserDto } from "src/dtos/user/delete.user.dto";
import { get } from "http";
import { EditFullname } from "src/dtos/user/edit.fullname.dto";

@Controller('api/user')
export class UserController{
    constructor(
        public userService:UserService,
        public photoService:PhotoService
    ){}

           //GET MY DATA
       @Get('myData/:email')
       getDataByEmail(@Param('email')email:string):Promise<User>{
        return this.userService.getByEmail(email)
       }




    //GET ALL USERS
    @Get()
    //Only for administrator
   @UseGuards(RoleCheckedGuard)
     @AllowToRoles("admin","super_admin")
    getAll():Promise<User[]>{
        return this.userService.getAll();
    }
     
    //GET ONE BY ID
    @Get(':id/getData')
    getById(@Param('id')userId:number):Promise<User | ApiResponse>{
        return this.userService.getById(userId)
    }

   // ADD NEW USER
@Post("addUser")
@UseGuards(RoleCheckedGuard)
@AllowToRoles("admin","super_admin")

addUser(@Body()data:AddUserDto):Promise<User | ApiResponse>{
    //return this.userService.add(data);
    return this.userService.add(data).catch(err => {
        throw new HttpException({
          message: /*err.message*/"User width these parameters already exist"
        }, HttpStatus.BAD_REQUEST);
      })
}


//EDIT USER EMAIL

  @Post(':id/changeEmail')
  editUserEmail(@Param("id")id:number,@Body()data:EditUserEmailDto):Promise<User | ApiResponse>{
  return this.userService.editUserEmail(id,data)
}


   //EDIT USER PASSWORD

   @Post(':id/changePassword')
   editUserPassword(@Param('id')id:number,@Body()data:EditUserDto):Promise<User | ApiResponse>{
       return  this.userService.editUserPassword(id,data);
   }

   @Post(':id/changeFullname')
   editUserFullname(@Param('id')id:number,@Body()data:EditFullname):Promise<User | ApiResponse>{
       return  this.userService.editFullname(id,data);
   }



   //DELETE USER 

   @Delete(':id')
   @UseGuards(RoleCheckedGuard)
     @AllowToRoles("admin","super_admin")
    
  remove(@Param('id') id: number):void {
    this.userService.removeuser(id)
    //.catch(err => {
      //  throw new HttpException({
        //  message: /*err.message*/"User not exist"
        //}, HttpStatus.NOT_FOUND);
      //})
  }


//UPLOAD USER PROFILE PHOTO

@Post(':id/uploadPhoto')
  @UseInterceptors(    //file interceptors(presretac)
     FileInterceptor("photo",{

       storage:diskStorage({
           destination:StorageConfig.photoDestination,
           filename:(req,file,callback)=>{

            let original:string = file.originalname;

            let normalized = original.replace(/\s+/g,'s')//replace all spaces with a  dash
            
            //make date like "20200426"
            let current = new Date();
            let datePart = '';

            datePart+= current.getFullYear().toString();
            datePart+= (current.getMonth() + 1).toString();
            datePart+=current.getDate().toString();
             

            //make random number like "9547612539"

            
            let randomPart:string =
             new Array(10)
           .fill(0)
            .map(e => (Math.random()* 9).toFixed(0).toString()).join('')
      let fileName = datePart + '-' + randomPart + '-' + normalized

    callback(null, fileName)

           }
       }),
       fileFilter:(req,file,callback)=>{
           //check extension
           if(file.originalname.match(/\.(jpg | png)$/)){
               
            req.fileFilterError = "Bad file extension!"// for api response
            callback(null,false) //this is displayed in console
               return;
           }
           //check type of data (image/jpeg, image/png)
           if(!(file.mimetype.includes('jpeg') || file.mimetype.includes('png'))) {
            
           req.fileFilterError="Bad file content type!"
            callback(null,false)
          }

           callback(null,true)//if every is ok
       },
       limits:{
           files:1,
           fileSize:StorageConfig.photoMaxFileSize
       }
     }) 
  )

    
        //UPLOAD PHOTO
  async uploadPhoto(@Param('id')userId:number,@UploadedFile()photo,@Req()req):Promise<ApiResponse |Userphoto>{
    
  if(req.fileFilterError){
     unlinkSync(photo.path)
      return new ApiResponse("error",-4002,req.fileFilterError)
  }

  if(!photo){
      return new ApiResponse("error",-4002,"file not uploaded")
  }
 

  //check real mime type 
 const fileTypeResult = await fromFile(photo.path)
 
 if(!fileTypeResult){
     //delete uploaded file if content is not good
   
     unlinkSync(photo.path)
     return new ApiResponse("error",-4002,"Cannot not detect file type")
 } 

 const realMimeType = fileTypeResult.mime;
 if(!(realMimeType.includes("jpeg") || realMimeType.includes("png"))) {
    unlinkSync(photo.path)
   
    return new ApiResponse("error",-4002,"Bad file content type")
 }
 
   const newPhoto:Userphoto= new Userphoto();
   newPhoto.userId = userId;
   newPhoto.imagePath = photo.filename;
  
try{ 
   var savedPhoto = await this.photoService.add(newPhoto);
} catch (err) { // check if photo already exist
    if (err.code === 'ER_DUP_ENTRY') {
        return new ApiResponse("error",-4002,"This user already has a profile picture")
    } 
}
    if(!savedPhoto){ // if photo not uploaded
        unlinkSync(photo.path)
    return new ApiResponse("error",-4001,"upload filed")

   }

   return savedPhoto;
  }

  //DELETE PROFILE PHOTO

  //http://localhost:3000/api/user/2/deletePhoto
  @Delete(':userId/deletePhoto/:userPhotoId')
  public async deletePhoto(
      @Param('userId') userId:number,
      @Param('userPhotoId')userPhotoId:number,
      ){
          
        //chech if user exist

        const photo = await this.photoService.findOne({
            userId:userId,
            userPhotoId:userPhotoId,
        })
        if(!photo){
          throw new HttpException({
            message: /*err.message*/"Photo not found"
          }, HttpStatus.NOT_FOUND);
         }
 try{
         unlinkSync(StorageConfig.photoDestination + photo.imagePath)
 } catch(e){
     
 }
        const deleteResult = await this.photoService.delete(userPhotoId)

          if(deleteResult.affected == 0){  //How many photo have been deleted
            throw new HttpException({
              message: /*err.message*/"Photo not found"
            }, HttpStatus.NOT_FOUND);
          }  

          return new ApiResponse("ok",0,"One photo is deleted")
    
    }

    // If user is admin give him adminDashboard page

    @Get('whoIsUser')
    @UseGuards(RoleCheckedGuard)
    @AllowToRoles("admin","super_admin")
 
      checkWHoIsLoggedIn(){
      return "success"
    }

     
    //get user data based on token
    @Get('checkUser')
    getUserDataBasedOnToken(@Req()req){
      return this.userService.getById(req.token.userId)
    }


    //ACTIVATE OR DEACTIVATE USER

    @Post(':id/activateOrDeactivateUser')
    @UseGuards(RoleCheckedGuard)
    @AllowToRoles("admin","super_admin")
    deleteUser(@Param("id")id:number,@Body()data:DeleteUserDto){
      return this.userService.deleteUser(id,data)
    }


    //GET ALL USERS WHO IS NOT REMOVED

    @Get("getAllUsersForAdmin") // get user from token 
    @UseGuards(RoleCheckedGuard)
    @AllowToRoles("admin","super_admin")
   async getAllUsersForAdmin(@Req()req,@Param('id')clientId:number):Promise<User[]>{
      return await this.userService.getAllUserWhoNotRemoved(clientId, req.token.userId) // pass token from req (change this logic)
    }


      //GET ALL USERS WHO IS NOT REMOVED

      @Get("getAllUsersForAdminWihoutId") // get user from token 
      @UseGuards(RoleCheckedGuard)
      @AllowToRoles("admin","super_admin")
     async getAllUsersForAdminWithoutId():Promise<User[]>{
        return await this.userService.getAllUserWhoNotRemovedWithoutId() // pass token from req (change this logic)
      }


    //GET OUR USERS

    @Get(":userId/getOurUsers")
    @UseGuards(RoleCheckedGuard)
    @AllowToRoles("admin","super_admin")
    getOurUsersNotRemoved(@Param("userId")userId:number):Promise<User[]>{
      return this.userService.getAllOurUsersNotRemoved(userId)
    }


    
    //GET OUR USERS

    @Get("getAllUsersForAdminSeeAndSetValue")
    @UseGuards(RoleCheckedGuard)
    @AllowToRoles("admin","super_admin")
    getAllClientsUsersNotRemoved():Promise<User[]>{
      return this.userService.getAllUsersNotRemoved()
    }

  }