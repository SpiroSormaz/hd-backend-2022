import { Controller, Get, Param, UseGuards, Body, Req, Post, BadRequestException, Put, Patch, Delete } from "@nestjs/common";
import { TaskService } from "services/task/task.service";
import { ApiResponse } from "src/misc/api.response.class";
import { Task } from "entities/task.entity";
import { AllowToRoles } from "src/misc/allow.to.roles.descriptor";
import { AddTaskDto } from "src/dtos/task/add.task.dto";
import {Request}  from "express";
import { RoleCheckedGuard } from "src/misc/role.checker.guard";
import { ClientTask } from "entities/client-task.entity";
import { EditTaskStatusDto } from "src/dtos/task/changeTaskStatus.dto";
import { EditTaskTitleDto } from "src/dtos/task/changeTaskTitle.dto";
import { EditTaskContent } from "src/dtos/task/changeTaskContent.dto";
import { DeleteTaskDto } from "src/dtos/task/delete.task.dto";
import { GetAllMyTask } from "src/dtos/task/getAllMyTask.dto";
import { EditTaskContentAndTitle } from "src/dtos/task/changeTaskContentAndTitleTogether.dto";
import { ReadTaskDto } from "src/dtos/task/readTask.dto";
import { ReadTask } from "entities/read-task.entity";
import { FilterTaskDto } from "src/dtos/task/filter.task.dto";
import { AfterInsert } from "typeorm";
import { Interval } from "@nestjs/schedule";
import { FilterTaskForAdminDto } from "src/dtos/task/filterTaskForAdmin.dto";
import { FilterTaskWithStatusDto } from "src/dtos/task/filterTaskWithStatus.dto";




@Controller('api/task')
export class TaskContoller{
    constructor(
        private taskService:TaskService
        
    ){}
  
    

    //GET ONE BY ID
@Get('getOne/:id')
async getById(@Param('id')taskId:number):Promise<Task | ApiResponse>{
        return this.taskService.getOneById(taskId)
        

    }


    @Post("sendReadTask")
sendParametersInReadTaskTableForTask(@Req()req,@Body()data:ReadTaskDto):Promise<ReadTask | ApiResponse>{
  return this.taskService.sendParametersInReadTaskTable(data, req.token.userId)
}






    //ADD TASK
@Post('add')
async addTask(@Body()data:AddTaskDto,@Req()req:Request){
    return  this.taskService.addNewTask(data);

}


  //EDIT TASK STATUS

  @Post(":id/editStatus")
  async changeStatus(@Param('id')id:number,@Body()data:EditTaskStatusDto){
      return await this.taskService.changeTaskStatus(id,data.newStatus,data.editedStatus)
      
  }





  //EDIT TASK TITLE

  @Post(":id/editTitle")
  async changeTitle(@Param('id')id:number,@Body()data:EditTaskTitleDto){

    
      return await this.taskService.editTaskTitle(id,data);
  }



    //EDIT TASK CONTENT

    @Post(":id/editContent/")
    async changeContent(@Param('id')id:number,@Body()data:EditTaskContent){
        return await this.taskService.editTaskContent(id,data)
    }




    //EDIT TASK CONTENT AND TITLE TOGETHER

    @Post(":id/editTaskContentAndTitle")
    async changeTaskContentAndTitle(@Param('id')id:number,@Body()data:EditTaskContentAndTitle){
        return await this.taskService.editTaskContentAndTitleTogether(id,data)
    }
  

    

    //GET ALL MY TASKS
@Get(":id/allTasks")
async getMyTasks(@Param("id")clientId:number):Promise<Task[] | ApiResponse>{
   const allTasks =  await this.taskService.getAllById(clientId);
  
 

   return allTasks;
   }



    //GET ALL MY TASKS FOR ADMIN
@Get(":id/allTasksForAdmin")
@UseGuards(RoleCheckedGuard)
@AllowToRoles("admin")
@AllowToRoles("super_admin")
async getMyTasksForAdmin(@Param("id")clientId:number):Promise<Task[] | ApiResponse>{
   const allTasks =  await this.taskService.getAllById(clientId);
  
 

   return allTasks;
   }



 
 
   @Post('getTaskForNotification')
   async find(@Body('userId')userId:number): Promise <Task[]>{
       return await this.taskService.findTaskForNotification(userId);
   
}


@Post("getTaskForNotificationForAdmin")
async findNotificationForAdmin(@Req()req):Promise<Task[]>{
    return await this.taskService.findTaskForNotificationForAdmin(req.token.userId)
}
 







   //GET ALL MY  TASK WITH THE STATUS I WANT
 
   @Get("/:id/:status")
async getAllMyTaskWithStatus(@Param("status")status:string,@Param("id")clientId:number,):Promise<Task[]>{
   
   return await this.taskService.getAllMyTaskStatusIWant(status,clientId);
     
}

 @Get('/:status')
 async getAllTasksWithSatusTwoVer(@Param("status")status:string):Promise<Task[]>{
     return await this.taskService.getAllMyTaskStatusIWantVerTwo(status)
 }




 //DELETE TASK


 @Post(':id/deleteTask')
 remove(@Param('id') id: number,@Body()data:DeleteTaskDto):void {
   this.taskService.removeTask(id,data)
   
 }




 //Get all my tasks with username param

     //GET ALL MY TASKS
@Post("getAllMyTasks")
  async getAllMyTasks(@Body()data:GetAllMyTask){
   
    return await this.taskService.getAllMyTask(data);
  
  
}


   //filter task

   @Post("filterTask")
   async filterTask(@Body()data:FilterTaskDto):Promise<Task[]>{
       return await this.taskService.search(data)
   }


   //filter task for admin

   @Post("filterTaskForAdmin")
       filterTaskForAdmin(@Body()data:FilterTaskForAdminDto):Promise<Task[]>{
            return this.taskService.filterForAdmin(data)
       }
   


   //GET ALL TASKS WHO IS NOT REMOVED (FOR ADMIN)

   @Get()
   async allTasksForSee():Promise<Task[] | Task>{
       return  await this.taskService.getAllTasksForShow()
   }

//filter task with status for client
   @Post("filterTaskForClient")
   async filterTaskWithStatusForClient(@Body()data:FilterTaskWithStatusDto):Promise<Task[]>{
       return await this.taskService.searchTaskForClient(data)
   }




}
