import { Controller, Get, Param, Post, Body, UseGuards } from "@nestjs/common";
import { ClientService } from "services/client/client.service";
import { Client } from "entities/client.entity";
import { getConnectionName } from "@nestjs/typeorm";
import { AddClientDto } from "src/dtos/client/add.client.dto";
import { DeleteClientDto } from "src/dtos/client/delete.client.dto";
import { AllowToRoles } from "src/misc/allow.to.roles.descriptor";
import { RoleCheckedGuard } from "src/misc/role.checker.guard";

@Controller('api/client')
export class ClientController{

    constructor(
        private clientService:ClientService
    ){}


    //GET ALL CLIENTS
    
    @Get('getAllClients')
    @UseGuards(RoleCheckedGuard)
    @AllowToRoles("admin")
    @AllowToRoles("super_admin")
    getAll():Promise<Client[]>{
        return this.clientService.getAllClient();
    }


    //ADD NEW CLIENT
    
    @Post('addClient')
    @UseGuards(RoleCheckedGuard)
    @AllowToRoles("admin")
    @AllowToRoles("super_admin")
    addNewClient(@Body()data:AddClientDto):Promise<Client>{
        return this.clientService.addNewClient(data)
    }


    //GET CLIENT NAME

    @Get(':id/getClientName')
      async getClientName(@Param("id")clientId:number):Promise<Client>{
           return await this.clientService.getClientName(clientId)
       } 


       //DELETE CLIENT

       @Post(':id/activateOrDeactivateClient')
       @UseGuards(RoleCheckedGuard)
       @AllowToRoles("admin")
       @AllowToRoles("super_admin")
       remove(@Param('id') clientId: number,@Body()data:DeleteClientDto):void {
         this.clientService.deleteClient(clientId,data)
         
       }



       @Get("getAllClients")
       @UseGuards(RoleCheckedGuard)
       @AllowToRoles("admin")
       @AllowToRoles("super_admin")
        getAllCLientsForAdmin():Promise<Client[]>{
           return this.clientService.getAllClientsForAdmin()

       

       }

    //get clients and users all 
      
    @Get("getAllClients")
       @UseGuards(RoleCheckedGuard)
       @AllowToRoles("admin")
       @AllowToRoles("super_admin")
        getAllClientsForAdmin():Promise<Client[]>{
           return this.clientService.getAllClients()

       

       }
    
}