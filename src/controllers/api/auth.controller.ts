import { Controller, Post, Body,Req, BadRequestException, NotFoundException} from "@nestjs/common";
import { UserService } from "services/user/user.service";
import { UserLoginDto } from "src/dtos/user/login.user.dto";
import { ApiResponse } from "src/misc/api.response.class";
import {createHash}  from "crypto";
import { LoginInfoUserDto } from "src/dtos/user/login.info.user.dto";
import {sign} from "jsonwebtoken";
import { JwtDataUserDto } from "src/dtos/user/jwt.data.user.dto";
import {Request} from "express";
import { jwtSecret } from "config/jwt.secret";

@Controller('auth')
export class AuthController{
    constructor(
        public userService:UserService
    ){}
@Post('user/login')
   async doLogin(@Body()data:UserLoginDto,@Req()req:Request):Promise<ApiResponse | LoginInfoUserDto>{
      const user = await this.userService.getByEmail(data.email);
    if(!user){
        throw new NotFoundException('wrong parameters');
       // return new Promise(resolve=>resolve(new ApiResponse("error",-3001,"user not exist")))
        
     }

     const passwordHash = createHash('sha512');
     passwordHash.update(data.password);
     const passwordHashString = passwordHash.digest('hex').toUpperCase();

     if(user.passwordHash !== passwordHashString){
        //return new Promise(resolve=>resolve(new ApiResponse("error",-3001,"password is wrong")))
        throw new BadRequestException('wrong parameters');
        
     }


     //TOKEN = JSON (userId,username,except time,ip, user agent)
    const jwtData = new JwtDataUserDto()
    
        jwtData.userId = user.userId;
        jwtData.email = user.email;
        jwtData.firstname = user.firstname;
        jwtData.lastname = user.lastname;
        jwtData.clientId = user.clientId;
        jwtData.privileges = user.privileges
   
        let current = new Date();
        current.setDate(current.getDate() + 30);
        const expirationTimeStamp = current.getTime() / 1000; 
         jwtData.exp = expirationTimeStamp;
         jwtData.ip = req.ip.toString();
         jwtData.ua = req.headers["user-agent"];
     let token: string= sign(jwtData.toPlainObject(),jwtSecret);

   

     const responseObject =  new LoginInfoUserDto( // ako ovo vracamo onda moramo gore da stavimo da vracamo ovo ili api response
         user.userId,
         user.email,
         user.firstname,
         user.lastname,
         user.clientId,
         user.privileges,
        
        
         token
     );

     return new Promise(resolve =>resolve(responseObject))

     }
    
    }

   