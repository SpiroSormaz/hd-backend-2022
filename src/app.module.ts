import { Module,NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import { Client } from '../entities/client.entity';
import { Comment} from '../entities/comment.entity';
import { UserController } from './controllers/api/user.controller';
import { UserService } from 'services/user/user.service';
import { AuthController } from './controllers/api/auth.controller';
import { DatabaseConfiguration } from 'config/database.configuration';
import { AuthMiddleware } from './middlewares/auth.Middleware';
import { PhotoService } from 'services/photo/photo.service';
import { Userphoto } from 'entities/userPhoto.entity';
import { ClientController } from './controllers/api/client.controller';
import { ClientService } from 'services/client/client.service';
import { Task } from 'entities/task.entity';
import { Document } from 'entities/document.entity';
import { ClientTask } from 'entities/client-task.entity';
import { User } from 'entities/user.entity';
import { TaskContoller } from './controllers/api/task.controller';
import { TaskService } from 'services/task/task.service';
import { CommentController } from './controllers/api/comment.controller';
import { CommentService } from 'services/comment/comment.service';
import { DocumentService } from 'services/document/document.service';
import { DocumentController } from './controllers/api/document.controller';
import { ForgottenPasswordController } from './controllers/api/forgottenPassword.controller';
import {MailerModule} from '@nestjs-modules/mailer'
import { MailConfig } from 'config/mail.config';
import { ReadComment } from 'entities/read-comment.entity';
import { ReadTask } from 'entities/read-task.entity';
import { ScheduleModule } from '@nestjs/schedule';
import { AppGateway } from 'services/socket/app.gateway';
import { Hello } from './controllers/api/hello.controller';


@Module({
  imports: [
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot({
      type:'mysql',
      host:DatabaseConfiguration.hostname,
      port:3306,
      username:DatabaseConfiguration.username,
      password:DatabaseConfiguration.password,
      database:DatabaseConfiguration.database,
      entities:[ClientTask,Client,Comment,Document,Task,Userphoto,User,ReadComment,ReadTask
      ],
    
    }),
    MailerModule.forRoot({
     //example:  smtps://username:password@smtp.gmail.com
      transport:'smtps://' + MailConfig.username + ':' + MailConfig.password + '@' + MailConfig.hostname,
      defaults:{
        from:'help-desk-BTB'
      }
    }),
    TypeOrmModule.forFeature([ //navodimo nase Feature tj kao entitete
      ClientTask,Client,Comment,Document,Task,Userphoto,User,ReadComment,ReadTask
]
   )
  ],
  controllers: [AppController,UserController,AuthController,ClientController,TaskContoller,CommentController,DocumentController,ForgottenPasswordController,Hello],
  providers: [UserService,PhotoService,ClientService,TaskService,CommentService,DocumentService,AppGateway],

  exports:[  // must be exports because we use it in auth.middleware file
    UserService,
    
  ],
})
export class AppModule implements NestModule { // register midlware but first must put code (implement NestModule)
  configure(consumer:MiddlewareConsumer) {
    consumer.apply(AuthMiddleware)
    .exclude('auth/*') // not here apply because we not need token for login 
    // list of routes where this should be applied
    .forRoutes('api/*')
  }
} 
