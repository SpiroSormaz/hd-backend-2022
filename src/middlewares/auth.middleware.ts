import { NestMiddleware, HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { NextFunction,Request,Response } from "express";
import { UserService } from "services/user/user.service";
import * as jwt from "jsonwebtoken";
import { JwtDataUserDto } from "src/dtos/user/jwt.data.user.dto";
import {jwtSecret} from "config/jwt.secret";


@Injectable()
export class AuthMiddleware implements NestMiddleware{
    constructor(private readonly userService:UserService){}
    
    async use(req:Request,res:Response,next:NextFunction){
      if(!req.headers.authorization){
           throw new HttpException("token not founds",HttpStatus.UNAUTHORIZED);
      }

      
    

      const token = req.headers.authorization;
      
      const tokenParts = token.split(' ');
      if(tokenParts.length !==2){
        throw new HttpException("Bad token",HttpStatus.UNAUTHORIZED)
      }
      const tokenString = tokenParts[1];

      let jwtData:JwtDataUserDto; 
      try{ //check is token valid or not
       jwtData=jwt.verify(tokenString,jwtSecret);

      }catch(e){
        throw new HttpException("bad token  found",HttpStatus.UNAUTHORIZED);


}
      if(!jwtData){
        throw new HttpException("bad token  found",HttpStatus.UNAUTHORIZED);

      }

     

      if(jwtData.ip !== req.ip.toString()){
      throw new HttpException("bad token  found",HttpStatus.UNAUTHORIZED);
    
      }

      if(jwtData.ua !== req.headers["user-agent"]){
        throw new HttpException("bad token  found",HttpStatus.UNAUTHORIZED);
      }

      const user  = await this.userService.getById(jwt.userId);

      if(!user){
        throw new HttpException("Account not found",HttpStatus.UNAUTHORIZED);

      }

      
      const currentTimeStamp = new Date().getTime() / 1000;
    
      if(currentTimeStamp >= jwtData.exp){
        throw new HttpException("The token has expired",HttpStatus.UNAUTHORIZED);
      }

      req.token = jwtData;
      next();//if everything is ok
    }
}