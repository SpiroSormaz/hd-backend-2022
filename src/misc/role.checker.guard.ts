import { Injectable, CanActivate, ExecutionContext} from "@nestjs/common";
import { Observable } from "rxjs";
import {Request} from "express";
import { Reflector } from "@nestjs/core";


@Injectable()

export class RoleCheckedGuard implements CanActivate {
   constructor(private reflector:Reflector){}

    canActivate(context:ExecutionContext):boolean | Promise<boolean> | Observable<boolean>{
 const req : Request = context.switchToHttp().getRequest();
 const role = req.token.privileges;

 const allowedToRoles = this.reflector.get<("super_admin" | "admin" | "no_privilege")[]>('allow_to_roles',context.getHandler())

     if(!allowedToRoles.includes(role) && allowedToRoles){

        return false;

     }

     return true;

        //true - enable to use api
        //false - disable to use api
    }null
}