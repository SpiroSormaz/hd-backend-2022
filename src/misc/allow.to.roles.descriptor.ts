import { SetMetadata } from "@nestjs/common"

export const AllowToRoles = (...roles:("super_admin" | "admin"| "no_privilege")[])=>{
    return SetMetadata('allow_to_roles',roles);
}