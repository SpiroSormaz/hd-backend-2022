import { JwtDataUserDto } from "src/dtos/user/jwt.data.user.dto";

//system to have a token available in other pages 

declare module 'express'{
    interface Request {
        token:JwtDataUserDto
    }
}