import * as Validator from "class-validator";
export class EditTaskStatusDto{
    newStatus:"notassigned" | "assigned" | "paused" | "rejected" | "closed";
    
    
    @Validator.Length(1,50)
    editedStatus:string
}