import * as Validator from "class-validator";
export class  EditTaskContentAndTitle{
    @Validator.IsNotEmpty()
    @Validator.IsString()
    newTitle:string;
    @Validator.IsNotEmpty()
    @Validator.IsString()
    newContent:string;
     
    userId:number;

}