import * as Validator from "class-validator"

export class EditTaskContent{
    @Validator.IsNotEmpty()
    @Validator.IsString()
    newContent:string;


    userId:number;
}