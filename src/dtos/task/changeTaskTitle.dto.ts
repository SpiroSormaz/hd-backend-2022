import * as Validator from "class-validator";

export class EditTaskTitleDto{
    @Validator.IsNotEmpty()
    @Validator.IsString()
    newTitle:string;

  
    userId:number;
}