import * as Validator from "class-validator";

export class AddTaskDto{
    
    @Validator.IsString()
    @Validator.IsNotEmpty()
    
    taskTitle:string;
    
    @Validator.IsString()
    @Validator.IsNotEmpty()
    taskContent:string;

    @Validator.IsIn( ["notassigned" , "assigned" , "paused" , "rejected" , "closed"])
    status:"notassigned" | "assigned" | "paused" | "rejected" | "closed";
    taskId:number;
    clientId:number;


     
   @Validator.IsNotEmpty()
   userId:number;


   
    

}