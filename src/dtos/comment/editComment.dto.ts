import * as Validator from "class-validator"

export class EditCommentDto{

    @Validator.IsString()
    @Validator.IsNotEmpty()
    newContent:string;
    userId:number;
}