import * as Validator from "class-validator";


export class EditUserEmailDto{
    
    @Validator.IsEmail()
    oldEmail:string;
    @Validator.IsEmail()
    newEmail:string;
}