import * as Validator from "class-validator";
import { IsEmail } from "class-validator";

export class EditFullname{

    @Validator.IsNotEmpty()
    @Validator.IsString()
    firstname:string;
    
    @Validator.IsNotEmpty()
    @Validator.IsString()
    lastname:string;
    
}