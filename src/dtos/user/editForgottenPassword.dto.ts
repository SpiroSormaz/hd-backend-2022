import * as Validator from "class-validator";

export class EditForgottenpassword{
 
    @Validator.IsEmail()
    email:string;

}