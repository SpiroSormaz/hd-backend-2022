import * as Validator from "class-validator";
import { IsEmail } from "class-validator";

export class EditUserDto{

    @Validator.IsNotEmpty()
    @Validator.Length(6,128)
    password:string;
    
    @Validator.IsNotEmpty()
    @Validator.Length(6,128)
    oldPassword:string;
    
}