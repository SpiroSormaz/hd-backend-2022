export class LoginInfoUserDto{
    userId:number;
    firstname:string;
    lastname:string;
    clientId:number;
    email:string;
    privileges:string;
    token:string;
    

    constructor(id:number,em:string,fn:string,ln:string,cId:number,privileges:string,jwt:string){

        this.userId = id;
         this.email = em;
        this.firstname = fn;
        this.lastname = ln;
        this.clientId = cId;
        this.privileges = privileges;
        this.token = jwt;

    }
}