import * as Validator from "class-validator";

export class AddUserDto{
  
    
    @Validator.IsNotEmpty()
    @Validator.IsString()
    firstname:string;
    
    @Validator.IsNotEmpty()
    @Validator.IsString()
    lastname:string;
    
    @Validator.IsNotEmpty()
    @Validator.Length(6,128)
    password:string;
   
    @Validator.IsEmail()
    email:string;
    

    
    clientId:number | null;

    @Validator.IsIn( ["super_admin", "admin", "no_privilege"])
    privileges:"super_admin" | "admin" | "no_privilege";
    
    
 
   

}