import * as Validator from "class-validator";
export class UserLoginDto {
    @Validator.IsNotEmpty()
    @Validator.IsString()
    @Validator.IsEmail()
    email:string


    @Validator.IsNotEmpty()
    @Validator.IsString()
    @Validator.Length(6,128,{
        message:"password must be longer than or equal to 6 characters"
    })
    password:string;
    
    
}