export class JwtDataUserDto{
   
    userId : number;
    email:string;
    firstname:string;
    lastname:string;
    clientId:number;
    privileges:"super_admin" | "admin" | "no_privilege"
    exp:number; //unix timestamp
    ip: string;
    ua: string;

    toPlainObject(){
        return {
         
          userId : this.userId,
          email:this.email,
          privileges:this.privileges,
          firstname:this.firstname,
          lastname:this.lastname,
          clientId:this.clientId,
          exp:this.exp,
          ip:this.ip,
          ua:this.ua
        }
    }
}