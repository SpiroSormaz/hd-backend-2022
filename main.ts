import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { StorageConfig } from 'config/storage.config';
import { ValidationPipe } from '@nestjs/common';
import { CommentDocumentStorageConfig } from 'config/commentDocumentStorage.config';
import { TaskDocumentStorageConfig } from 'config/taskDocumentStorage.config';
import { AppModule } from 'src/app.module';
let mysql =require('mysql')

async function bootstrap() {
  
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.enableCors({
    origin: 'http://localhost:8080',
    credentials: true,
  });
  //http://localhost:3000/assets/user_photos/nameOfImg.png or jpg

  app.useStaticAssets(StorageConfig.photoDestination,{
    prefix:StorageConfig.urlPrefix,
    maxAge:StorageConfig.maxAge,//seven days
    index:false,
  })

  app.useStaticAssets(CommentDocumentStorageConfig.documentDestination,{
    prefix:CommentDocumentStorageConfig.urlPrefix,
    maxAge:CommentDocumentStorageConfig.maxAge,//seven days
    index:false,
  })


  app.useStaticAssets(TaskDocumentStorageConfig.documentDestination,{
    prefix:TaskDocumentStorageConfig.urlPrefix,
    maxAge:TaskDocumentStorageConfig.maxAge,//seven days
    index:false,
  })
 

//data validator
app.useGlobalPipes(new ValidationPipe());

const port = process.env.Port || 3000;


 //1433 port 
//ip address 192.168.100.242
// user     insolutions\sqlins
//pass     kUpo0C@jLSzAD5!LaSLRKI|qn
   
  await app.listen(port);
  
}
bootstrap();
